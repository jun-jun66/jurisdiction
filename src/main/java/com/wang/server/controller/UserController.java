package com.wang.server.controller;

import com.wang.server.common.annotation.PreAuthorize;
import com.wang.server.common.result.R;
import com.wang.server.entity.User;
import com.wang.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    //为false 此接口放行 不做权限校验
    @PreAuthorize(false)
    @PostMapping
    public R login(@RequestBody User user){
        return userService.login(user);
    }

}
