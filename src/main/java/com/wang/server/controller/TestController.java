package com.wang.server.controller;


import com.wang.server.common.annotation.PreAuthorize;
import com.wang.server.common.result.R;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/test")
public class TestController {

    //需要验证  权限码为test::01
    @PreAuthorize(value = true , permission = "test::01")
    @GetMapping("/demo01")
    public R test01(){
        String s = "云曦";
        return R.ok().message(s);
    }

    //需要验证  权限码为test::02
    @PreAuthorize(value = true , permission = "test::02")
    @GetMapping("/demo02")
    public R test02(){
        return R.ok().message("风铃");
    }

}
