package com.wang.server.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.wang.server.common.result.R;
import com.wang.server.entity.Role;
import com.wang.server.entity.User;

import java.util.List;


public interface UserService extends IService<User> {
    User getOne(String username);

    List<Role> getRolesById(int id);

    R login(User user);
}
