package com.wang.server.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.wang.server.common.config.JwtTokenUtil;
import com.wang.server.common.result.R;
import com.wang.server.common.result.ResultEnum;
import com.wang.server.dao.UserDao;
import com.wang.server.entity.Role;
import com.wang.server.entity.User;
import com.wang.server.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class UserServiceImpl extends ServiceImpl<UserDao , User> implements UserService {

    @Autowired
    private UserDao userDao;

    @Value("${jwt.tokenHead}")
    private String tokenHead;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Override
    public User getOne(String username) {
        User u = getOne(new QueryWrapper<User>().lambda().eq(User::getUsername, username));
        return u;
    }

    @Override
    public List<Role> getRolesById(int id) {
        return userDao.getRolesById(id);
    }


    @Override
    public R login(User user) {
        User u = this.getOne(user.getUsername());
        if(!user.getPassword().equals(u.getPassword())){
            return R.error().resultEnum(ResultEnum.USER_LOGIN_FAILED);
        }
        //生成token
        String token = jwtTokenUtil.generateToken(u);
        Map<String,Object> tokenMap = new HashMap<>();
        tokenMap.put("token",token);
        tokenMap.put("tokenHead",tokenHead);
        return R.ok().resultEnum(ResultEnum.LOGIN_SUCCESS).data("token",tokenMap);
    }


}
