package com.wang.server.common.aspect;

import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.wang.server.common.annotation.PreAuthorize;
import com.wang.server.common.result.R;
import com.wang.server.common.result.ResultEnum;
import com.wang.server.common.util.UserDetail;
import com.wang.server.entity.PermissionRoleDto;
import com.wang.server.entity.Role;
import com.wang.server.entity.User;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.lang.reflect.Method;
import java.util.List;

/**
 * aop
 */
@Aspect
@Component
public class PermissionAspect {

    @Autowired
    private UserDetail userDetail;

    /**
     * 切点   切自定义的注解
     */
    @Pointcut("@annotation(com.wang.server.common.annotation.PreAuthorize)")
    public void pre() {
    }

    /**
     * 环绕增强
     * @param p
     * @return
     * @throws Throwable
     */
    @Around("pre()")
    public Object around(ProceedingJoinPoint p) throws Throwable {
        MethodSignature signature = (MethodSignature) p.getSignature();
        //获取方法
        Method m = signature.getMethod();
        //获取方法上的注解
        PreAuthorize annotation = m.getAnnotation(PreAuthorize.class);
        //获取注解中的状态 false直接放行
        if(!annotation.value()){
            //执行目标方法 也就是切的那个方法
            return p.proceed();
        }
        //获取权限码
        String permission = annotation.permission();
        //如果注解中没有   权限不足
        if(StringUtils.isBlank(permission)){
            return R.error().resultEnum(ResultEnum.ILLEGAL_OPERATION);
        }
        //获取权限  角色对应
        List<PermissionRoleDto> resourceRoleList = userDetail.getResourceRoleList();
        for (PermissionRoleDto permissionRoleDto : resourceRoleList) {
            //如果有  则去判断角色
            if(permissionRoleDto.getPermissionName().equals(permission)){
                //获取这个权限所需要的角色
                List<String> roleList = permissionRoleDto.getRoleList();
                //获取当前用户
                User user = userDetail.getThreadLocal().get();
                if(user == null){
                    return R.error().resultEnum(ResultEnum.USER_NEED_AUTHORITIES);
                }
                //获取用户角色
                List<Role> roles = user.getRoles();
                if(roles.isEmpty()){
                    return R.error().resultEnum(ResultEnum.ILLEGAL_OPERATION);
                }
                //比对用户角色 和 该接口权限码所需的角色是否相同
                for (Role role : roles) {
                    //如果相同
                    if(roleList.contains(role.getRoleName())){
                        //执行目标方法 也就是切的那个方法
                        Object o = p.proceed();
                        return o;
                    }
                }
            }
        }
        //否则  权限不足  非法操作
        return R.error().resultEnum(ResultEnum.ILLEGAL_OPERATION);
    }

}
