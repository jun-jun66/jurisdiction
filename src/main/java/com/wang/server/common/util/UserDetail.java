package com.wang.server.common.util;

import com.wang.server.dao.RoleDao;
import com.wang.server.entity.PermissionRoleDto;
import com.wang.server.entity.User;
import lombok.Data;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import javax.annotation.PostConstruct;
import java.util.List;

/**
 * 用来保存 ：
 *   用户（threadLocal） ThreadLocal：每个线程之间相互隔离数据  做到线程安全  数据只会保存到当前线程中
 *   权限角色对应（resourceRoleList）
 */
@Component
@Data
public class UserDetail {


    @Autowired
    private RoleDao roleDao;

    private ThreadLocal<User> threadLocal = new ThreadLocal<>();

    /**
     * 资源角色列表
     */
    public List<PermissionRoleDto> resourceRoleList;

    /**
     * 该注解标注的方法在bean初始化中会调用
     * 目的去数据库中查询接口对应何种角色才能访问
     * 保存到resourceRoleList中  以便aop中取用
     */
    @PostConstruct
    public void loadDataSource() {
        resourceRoleList = roleDao.listPermissionRoles();
        System.out.println(resourceRoleList);
    }

}
