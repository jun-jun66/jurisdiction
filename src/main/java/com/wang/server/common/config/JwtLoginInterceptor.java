package com.wang.server.common.config;

import com.wang.server.common.util.UserDetail;
import com.wang.server.entity.Role;
import com.wang.server.entity.User;
import com.wang.server.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * 拦截器
 *  作用：  如果有token就解析登录  并且保存用户到UserDetail 中的 threadLocal
 */
@Slf4j
@Configuration
public class JwtLoginInterceptor implements HandlerInterceptor {

    @Value("${jwt.tokenHeader}")
    private String tokenHeader;
    @Value("${jwt.tokenHead}")
    private String tokenHead;
    @Autowired
    private JwtTokenUtil jwtTokenUtil;
    @Autowired
    private UserService userService;
    @Autowired
    private UserDetail userDetail;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        String authHeader = httpServletRequest.getHeader(tokenHeader);
        //存在token
        if (null != authHeader) {
            if (authHeader.startsWith(tokenHead)) {
                String token = authHeader.substring(tokenHead.length());
                //解析token  获取用户名
                String username = jwtTokenUtil.getUserNameByToken(token);
                //查找user
                User u = userService.getOne(username);
                if(u != null){
                    //查找用户角色
                    List<Role> roles = userService.getRolesById(u.getId());
                    if(!roles.isEmpty()){
                        u.setRoles(roles);
                    }
                    //添加用户到userDetail
                    userDetail.getThreadLocal().set(u);
                }
            }
        }
        return true;
    }


    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        //从userDetail 中把 threadLocal中删除
        //ThreadLocal做到数据只能在当前线程中访问  但是tomcat是个线程池  线程是可以复用的  所以要删除当前线程中的用户数据
        userDetail.getThreadLocal().remove();
    }
}
