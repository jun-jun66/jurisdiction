package com.wang.server.common.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 跨域解决
 * @program: blogServer
 * @description: mvc
 * @author: Mr.Wang
 * @create: 2021-12-05 12:05
 **/
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

    @Autowired
    private JwtLoginInterceptor jwtLoginInterceptor;

    @Override
    public void addCorsMappings(CorsRegistry registry) {
            registry
                    //允许访问的接口地址
                    .addMapping("/**")
                    //允许发起跨域访问的域名
                    .allowedOriginPatterns("*")
                    //允许跨域访问的方法
                    .allowedMethods("GET", "HEAD", "POST", "PUT", "DELETE", "OPTIONS", "TRACE")
                    //是否带上cookie信息
                    .allowCredentials(true)
                    .maxAge(3600)
                    .allowedHeaders("*");

        }

    /**
     * 添加拦截器
     * @param registry
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(jwtLoginInterceptor);
    }
}