package com.wang.server.common.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PreAuthorize {

    /**
     * false 跳过认证  接口可直接访问
     * true  需要认证  接口要判断权限
     * @return
     */
    boolean value();

    /**
     * 接口权限码
     * @return
     */
    String permission() default "";
}
