package com.wang.server.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.*;
import lombok.experimental.Accessors;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
@Builder
public class User {

    @TableId(value = "id", type = IdType.AUTO)
    private int id;

    private String username;

    private String password;

    @TableField(exist = false)
    private List<Role> roles;

}
