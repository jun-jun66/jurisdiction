package com.wang.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.server.entity.Role;
import com.wang.server.entity.User;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface UserDao extends BaseMapper<User> {
    List<Role> getRolesById(int id);
}
