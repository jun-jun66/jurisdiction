package com.wang.server.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wang.server.entity.PermissionRoleDto;
import com.wang.server.entity.Role;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface RoleDao extends BaseMapper<Role> {

    List<PermissionRoleDto> listPermissionRoles();


}
